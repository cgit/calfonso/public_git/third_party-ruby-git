NAME 		:= git
SPECFILE	= extra/$(NAME).spec
VERSION         := $(shell rpm -q --qf "%{VERSION}\n" --specfile $(SPECFILE)| head -1)
RELEASE         := $(shell rpm -q --qf "%{RELEASE}\n" --specfile $(SPECFILE)| head -1)
UPSTREAM_NAME   = $(PROJECT)

TAG             = $(subst .,_,$(NAME)-$(VERSION)-$(RELEASE))

CVS             = cvs
RPMBUILD	= rpmbuild
INSTALL         = /usr/bin/install
INSTALL_DIR     = $(INSTALL) --verbose -d -m 755

RPM_TOPDIR	= /tmp/$(NAME)-$(VERSION)-$(RELEASE)-build
_RPM_OPTS	= --define "_topdir	    $(RPM_TOPDIR)" \
		  --define "_builddir	    %{_topdir}" \
		  --define "_sourcedir	    $(shell pwd)/pkg" \
		  --define "_specdir	    $(shell pwd)" \
		  --define "_rpmdir	    $(shell pwd)" \
		  --define "_srcrpmdir 	    $(shell pwd)" \
		  --define '_rpmfilename    %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm'
RPM_OPTS        = $(strip $(_RPM_OPTS))

rpm: clean gem $(RPM_TOPDIR) $(SPECFILE)
	$(RPMBUILD) --clean $(RPM_OPTS) -bb $(SPECFILE)

gem: 
	rake package

clean:
	@rm -rfv *~ *.rpm $(RPM_TOPDIR) $(ARCHIVE)
	rake clobber package

$(RPM_TOPDIR):
	@$(INSTALL_DIR) $@
